﻿using BattleTurn.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Abilities.Defensive
{
    public class Hex : Ability
    {
        public Hex()
        {
            Name = "Hex";
            Description = "If survive +1 MAT, if not +2 MAT to Ally";
            EffectType = EffectType.Defensive;
        }
        public override void Effect()
        {

        }
    }
}
