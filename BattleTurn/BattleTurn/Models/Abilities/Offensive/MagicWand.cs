﻿using BattleTurn.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Abilities.Offensive
{
    public class MagicWand : Ability
    {
        public MagicWand()
        {
            Name = "Magic Wand";
            Description = "Basic Attacks become Hybrid with +20% MAT";
            EffectType = EffectType.Offensive;
        }
        public override void Effect()
        {

        }
    }
}
