﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Base
{
    public abstract class Ability
    {
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public EffectType EffectType { get; set; }
        public abstract void Effect();
    }
    public enum EffectType
    {
        Offensive = 0,
        Defensive = 1
    }
}
