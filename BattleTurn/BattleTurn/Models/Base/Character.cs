﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Base
{
    public class Character
    {
        public Faction Faction { get; set; } = 0;
        public string Class { get; set; } = "Character";
        public Stats Stats { get; set; }
        public List<Move> Moves { get; set; }
        public Ability OffAbility { get; set; }
        public Ability DefAbility { get; set; }
    }
}
