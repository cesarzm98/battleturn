﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BattleTurn.Models.Base
{
    public abstract class Move
    {
        public Move(Stats stats)
        {
            CharacterStats = stats;
        }
        public string Name { get; set; } = string.Empty;
        public double Power { get; set; }
        public double Accurate { get; set; } = 100;
        public string Description { get; set; } = string.Empty;
        public Category Category { get; set; } = Category.Attack;
        public int Cooldown { get; set; } = 0;
        public int IfFails { get; set; } = 0;
        public abstract Stats EffectAttack();
        public abstract Stats EffectDebuff(Stats stats);
        public abstract List<Stats> EffectBoost(List<Stats> stats);
        public abstract Status EffectStatus();
        protected Stats CharacterStats { get; set; }
    }
    public enum Category
    {
        Attack = 0,
        Magic = 1,
        Hybrid = 2,
        Boost = 3,
        Debuff = 4,
    }
}
