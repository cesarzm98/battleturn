﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Base
{
    public class Stats
    {
        public double HP { get; set; }
        public double ATK { get; set; }
        public double MAT { get; set; }
        public double DEF { get; set; }
        public double MDF { get; set; }
        public double SPD { get; set; }
        public double LUK { get; set; }
    }
}
