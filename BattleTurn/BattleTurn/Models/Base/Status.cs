﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Base
{
    public enum Status
    {
        Nothing = 0,
        Confusion = 1,
        Paralyze = 2,
        Bleeding = 3,
        Evasion = 4
    }
}
