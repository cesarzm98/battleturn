﻿using BattleTurn.Models.Base;
using BattleTurn.Models.Base.Abilities.Defensive;
using BattleTurn.Models.Base.Abilities.Offensive;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Characters
{
    public class Wizard : Character
    {
        public Wizard()
        {
            Faction = Faction.Mage;
            Class = "Wizard";
            SetStats();
            SetAbilities();
        }
        private void SetStats()
        {
            Stats = new Stats()
            {
                HP = 17,
                ATK = 2,
                MAT = 5,
                DEF = 3,
                MDF = 4,
                SPD = 4,
                LUK = 5
            };
        }
        private void SetAbilities()
        {
            OffAbility = new MagicWand();
            DefAbility = new Hex();
        }
    }
}
