﻿using BattleTurn.Models.Abilities.Offensive;
using BattleTurn.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Moves
{
    public class BasicAttack : Move
    {

        public BasicAttack(Stats stats) : base(stats)
        {
            Power = CharacterStats.ATK;
            Name = "Basic Attack";
            Description = "An strike with a weapon";
        }
        public void CheckAbility(Ability ability)
        {
            if(ability.Name == new MagicWand().Name)
            {
                Category = Category.Hybrid;
                double addDMG = CharacterStats.MAT * .2;
                Power += addDMG;
            }
        }
        public override Stats EffectAttack()
        {
            return CharacterStats;
        }
        public override Stats EffectDebuff(Stats EnemyStats)
        {
            return CharacterStats;
        }
        public override List<Stats> EffectBoost(List<Stats> stats)
        {
            return new List<Stats>();
        }
        public override Status EffectStatus()
        {
            return Status.Nothing;
        }
    }
}
