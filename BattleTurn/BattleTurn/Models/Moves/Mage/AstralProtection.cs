﻿using BattleTurn.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Moves.Mage
{
    public class AstralProtection : Move
    {
        public AstralProtection(Stats stats) : base(stats)
        {
            Category = Category.Boost;
            Cooldown = 4;
        }
        public override List<Stats> EffectBoost(List<Stats> stats)
        {
            stats.ForEach(s => s.MDF += 2);
            return stats;
        }
        public override Stats EffectAttack()
        {

            return CharacterStats;
        }
        public override Stats EffectDebuff(Stats stats)
        {
            return CharacterStats;
        }
        public override Status EffectStatus()
        {
            return Status.Nothing;
        }
    }
}
