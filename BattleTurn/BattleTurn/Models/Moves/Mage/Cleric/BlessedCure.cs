﻿using BattleTurn.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Moves.Mage.Cleric
{
    public class BlessedCure : Move
    {
        public BlessedCure(Stats stats) : base(stats)
        {
            Category = Category.Boost;
            Cooldown = 3;
        }
        public override List<Stats> EffectBoost(List<Stats> stats)
        {
            stats.ForEach(s => s.HP += 5);
            return stats;
        }
        public override Stats EffectAttack()
        {
            return CharacterStats;
        }
        public override Stats EffectDebuff(Stats stats)
        {
            return CharacterStats;
        }
        public override Status EffectStatus()
        {
            return Status.Nothing;
        }
    }
}
