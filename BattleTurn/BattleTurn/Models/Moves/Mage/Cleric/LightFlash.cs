﻿using BattleTurn.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Moves.Mage.Cleric
{
    public class LightFlash : Move
    {
        public LightFlash(Stats stats) : base(stats)
        {
            Power = CharacterStats.MAT + 1;
            Accurate = 90;
            Category = Category.Magic;
            Cooldown = 2;
            IfFails = 1;
        }
        public override Status EffectStatus()
        {
            Random r = new Random();
            int chance = r.Next(4);
            if (chance > 1)
                return Status.Confusion;
            else return Status.Nothing;
        }
        public override Stats EffectAttack()
        {
            return CharacterStats;
        }
        public override List<Stats> EffectBoost(List<Stats> stats)
        {
            return new List<Stats>();
        }
        public override Stats EffectDebuff(Stats stats)
        {
            return CharacterStats;
        }
    }
}
