﻿using BattleTurn.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Moves.Mage.Wizard
{
    public class ArcanaCurse : Move
    {
        public ArcanaCurse(Stats stats) : base(stats)
        {
            Accurate = 90;
            Category = Category.Debuff;
            Cooldown = 4;
            IfFails = 2;
        }
        public override Stats EffectDebuff(Stats stats)
        {
            Random r = new Random();
            int hit = r.Next(100);
            if (hit < 90)
            {
                stats.ATK -= 1;
                stats.MAT -= 1;
            }
            else Cooldown -= IfFails;
            return stats;
        }
        public override Stats EffectAttack()
        {
            return CharacterStats;
        }
        public override List<Stats> EffectBoost(List<Stats> stats)
        {
            return new List<Stats>();
        }
        public override Status EffectStatus()
        {
            return Status.Nothing;
        }
    }
}
