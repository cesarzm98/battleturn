﻿using BattleTurn.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTurn.Models.Moves.Mage.Wizard
{
    public class MagicBurst : Move
    {
        public MagicBurst(Stats stats) : base(stats)
        {
            Power = CharacterStats.MAT + 3;
            Category = Category.Magic;
            Cooldown = 2;
        }
        public override Stats EffectAttack()
        {
            Random r = new Random();
            int chance = r.Next(2);
            if (chance == 0)
                CharacterStats.MAT++;
            return CharacterStats;
        }
        public override Stats EffectDebuff(Stats stats)
        {
            return CharacterStats;
        }
        public override List<Stats> EffectBoost(List<Stats> stats)
        {
            return new List<Stats>();
        }
        public override Status EffectStatus()
        {
            return Status.Nothing;
        }
    }
}
